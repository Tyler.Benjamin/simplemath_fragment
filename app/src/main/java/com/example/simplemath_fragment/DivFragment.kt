package com.example.simplemath_fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.text.style.TextAlign
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import com.example.simplemath_fragment.ui.theme.SimpleMath_FragmentTheme
import com.example.simplemath_fragment.viewmodel.div.DivViewModel
import kotlinx.coroutines.launch

class DivFragment: Fragment() {
    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                SimpleMath_FragmentTheme {
                }
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    var value: String by remember {
                        mutableStateOf("")
                    }

                    var value2: String by remember {
                        mutableStateOf("")
                    }

                    val viewModel = DivViewModel()
                    val findNavController = findNavController()


                    Box(
                        modifier = Modifier.wrapContentHeight(),
                        contentAlignment = Alignment.Center
                    ) {
                        Column {

                            Text(
                                text = "Result shown below",
                                textAlign = TextAlign.Center
                            )

                            TextField(
                                value = value,
                                onValueChange = {
                                    value = it
                                },
                                label = { Label2() },
                                modifier = Modifier.wrapContentSize()
                            )
                            TextField(
                                value = value2,
                                onValueChange = {
                                    value2 = it
                                },
                                label = { Label2() },
                                modifier = Modifier.wrapContentSize()
                            )
                            var substring = viewModel.result.collectAsState().value
                            Button(
                                onClick = {
                                    lifecycleScope.launch {
                                        viewModel.evaluateDivExpression(x = value, y = value2)
                                    }
                                    val result = (value.toInt() / value2.toInt()).toString()
                                    Toast.makeText(
                                        context,
                                        "$value / $value2 = $result",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                    findNavController.navigateUp()
                                }
                            ) {
                                Text(text = "Calculate")
//                                substring = viewModel.result.collectAsState().value
                            }
                        }
                    }
                }
            }
        }
    }
}
