package com.example.simplemath_fragment.repository

import com.example.simplemath_fragment.remote.MathService
import com.example.simplemath_fragment.remote.RetrofitObject

/**
 * Repository to evaluate math expressions.
 *
 * @constructor Create new [MathRepo]
 */
object MathRepo {
    private val service: MathService by lazy { RetrofitObject.getMathService() }

    /**
     * Evaluates mathematical expression passed in.
     *
     * @param expr mathematical expression to be evaluated
     * @return evaluation from mathematical expression
     */
    suspend fun evaluateExpression(expr: String): String {
        return service.evaluateExpression(expr).toString()
    }
}
