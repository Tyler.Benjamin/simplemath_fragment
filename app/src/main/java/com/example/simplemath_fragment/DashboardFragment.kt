package com.example.simplemath_fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.simplemath_fragment.ui.theme.SimpleMath_FragmentTheme

class DashboardFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                SimpleMath_FragmentTheme {
                    var findNavController = findNavController()

                    var displayResult: String by remember {
                        mutableStateOf("Result will show here")
                    }
                    // to nav fwd
                    // activity to activity -> Intent(this, ActivitiytoGoTo::class.java
                    // Fragment to Frag (with navigation component) -> findNAvcontroller().navigate(R.id.destination)

                    // to go back and finish()
                    //Activity -> finish()
                    //Fragment (with Nav COmponent) findNavController().navigateUp
                    Box(
                        modifier = Modifier.fillMaxSize()
                            .background(Color.LightGray),
                        contentAlignment = Alignment.Center
                    ) {
                        Column(modifier = Modifier.padding(vertical = 3.dp, horizontal = 10.dp)
                            .background(Color.White)
                            .wrapContentHeight()
                            .wrapContentWidth()) {
                            Text(text= "Let's Do some math", textAlign = TextAlign.Center, modifier = Modifier.padding(horizontal = 70.dp))
                            MainScreen(displayResult)
                            Row(modifier = Modifier.padding(vertical = 10.dp, horizontal = 20.dp)) {
                                Button(
                                    onClick = {findNavController.navigate(R.id.sumFragment)}

                                ) {
                                    Text(text = "+", fontSize = 25.sp)
                                }
                                Button(
                                    onClick = {findNavController.navigate(R.id.mulFragment)}
                                ) {
                                    Text(text = "x", fontSize = 25.sp)
                                }
                                Button(
                                    onClick = {findNavController.navigate(R.id.divFragment)}
                                ) {
                                    Text(text = "÷", fontSize = 25.sp)
                                }
                                Button(
                                    onClick = {findNavController.navigate(R.id.subFragment)}
                                ) {
                                    Text(text = "-", fontSize = 25.sp)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainScreen(result: String) {
    var value: String by remember {
        mutableStateOf("")
    }

    Box(modifier = Modifier.wrapContentHeight().width(300.dp).padding(horizontal = 10.dp), contentAlignment = Alignment.Center) {
        Text(text = "Result shown below", textAlign = TextAlign.Center)
        TextField(value = value, onValueChange = {
            println(it)
            value = it
        }, label = { Label(result) })
    }
}

@Composable
fun Label(result: String = "Show Result") {
    Text(
        text = result
    )
}

@Composable
fun Label2() {
    Text(
        text = "input number",
        fontSize = 8.sp
    )
}

//private fun navigateToDetails(charId: Int) {
//    val action =
//}
